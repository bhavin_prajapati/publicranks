/**
 * GET /tos
 * Tos page.
 */

exports.getTos = function(req, res) {
  res.render('tos', {
    title: 'Terms of Service'
  });
};