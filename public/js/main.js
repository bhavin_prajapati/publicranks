$(document).ready(function() {
	$(".youtube .dropdown-menu.amount li a").click(function(){
		$(".youtube .btn.amount").text($(this).text());
		$(".youtube .btn.amount").val($(this).text());
	});
	$(".youtube .dropdown-menu.payment li a").click(function(){
		$(".youtube .btn.payment").text($(this).text());
		$(".youtube .btn.payment").val($(this).text());
		if($(this).text() == "Paypal") {
			$(".youtube .paypal-payment").show();
			$(".youtube .stripe-payment").hide();
		} else {
			$(".youtube .paypal-payment").hide();
			$(".youtube .stripe-payment").show();
		}
	});

	$(".twitter .dropdown-menu.amount li a").click(function(){
		$(".twitter .btn.amount").text($(this).text());
		$(".twitter .btn.amount").val($(this).text());
	});
	$(".twitter .dropdown-menu.payment li a").click(function(){
		$(".twitter .btn.payment").text($(this).text());
		$(".twitter .btn.payment").val($(this).text());
		if($(this).text() == "Paypal") {
			$(".twitter .paypal-payment").show();
			$(".twitter .stripe-payment").hide();
		} else {
			$(".twitter .paypal-payment").hide();
			$(".twitter .stripe-payment").show();
		}
	});

	$(".facebook .dropdown-menu.amount li a").click(function(){
		$(".facebook .btn.amount").text($(this).text());
		$(".facebook .btn.amount").val($(this).text());
	});
	$(".facebook .dropdown-menu.payment li a").click(function(){
		$(".facebook .btn.payment").text($(this).text());
		$(".facebook .btn.payment").val($(this).text());
		if($(this).text() == "Paypal") {
			$(".facebook .paypal-payment").show();
			$(".facebook .stripe-payment").hide();
		} else {
			$(".facebook .paypal-payment").hide();
			$(".facebook .stripe-payment").show();
		}
	});

	$(".instagram .dropdown-menu.amount li a").click(function(){
		$(".instagram .btn.amount").text($(this).text());
		$(".instagram .btn.amount").val($(this).text());
	});
	$(".instagram .dropdown-menu.payment li a").click(function(){
		$(".instagram .btn.payment").text($(this).text());
		$(".instagram .btn.payment").val($(this).text());
		if($(this).text() == "Paypal") {
			$(".instagram .paypal-payment").show();
			$(".instagram .stripe-payment").hide();
		} else {
			$(".instagram .paypal-payment").hide();
			$(".instagram .stripe-payment").show();
		}
	});

	$(".linkedin .dropdown-menu.amount li a").click(function(){
		$(".linkedin .btn.amount").text($(this).text());
		$(".linkedin .btn.amount").val($(this).text());
	});
	$(".linkedin .dropdown-menu.payment li a").click(function(){
		$(".linkedin .btn.payment").text($(this).text());
		$(".linkedin .btn.payment").val($(this).text());
		if($(this).text() == "Paypal") {
			$(".linkedin .paypal-payment").show();
			$(".linkedin .stripe-payment").hide();
		} else {
			$(".linkedin .paypal-payment").hide();
			$(".linkedin .stripe-payment").show();
		}
	});

	$(".website .dropdown-menu.amount li a").click(function(){
		$(".website .btn.amount").text($(this).text());
		$(".website .btn.amount").val($(this).text());
	});
	$(".website .dropdown-menu.payment li a").click(function(){
		$(".website .btn.payment").text($(this).text());
		$(".website .btn.payment").val($(this).text());
		if($(this).text() == "Paypal") {
			$(".website .paypal-payment").show();
			$(".website .stripe-payment").hide();
		} else {
			$(".website .paypal-payment").hide();
			$(".website .stripe-payment").show();
		}
	});
});
